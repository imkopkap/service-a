package grpc

import (
	"context"

	proto "gitlab.com/imkopkap/service-a/delivery/grpc/proto"
	u "gitlab.com/imkopkap/service-a/usecase"
)

type Greeter interface {
	GetHello(ctx context.Context, req *proto.Request, rsp *proto.Response) error
}

type MyGreeter struct {
	usecase u.Usecase
}

func NewGreeter(usecase u.Usecase) Greeter {
	return &MyGreeter{
		usecase: usecase,
	}
}

func (g *MyGreeter) GetHello(ctx context.Context, req *proto.Request, rsp *proto.Response) error {
	rsp.Greeting = g.usecase.GetHello()
	// rsp.Greeting = "Hello"
	return nil
}
