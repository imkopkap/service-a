package main

import (
	"fmt"

	micro "github.com/micro/go-micro"
	proto "gitlab.com/imkopkap/service-a/delivery/grpc/proto"

	d "gitlab.com/imkopkap/service-a/delivery/grpc"
	r "gitlab.com/imkopkap/service-a/repository"
	u "gitlab.com/imkopkap/service-a/usecase"
)

func main() {
	// Declare service
	repo := r.NewMyRepository()
	usecase := u.NewMyUsecase(repo)

	// Declare delivery
	service := micro.NewService(
		micro.Name("go.micro.srv.greeter"),
	)

	service.Init()

	proto.RegisterGreeterHandler(service.Server(), d.NewGreeter(usecase))

	if err := service.Run(); err != nil {
		fmt.Println(err)
	}
}
