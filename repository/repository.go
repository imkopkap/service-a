package repository

type Repository interface {
	GetHello() string
}

type MyRepository struct{}

func NewMyRepository() Repository {
	return &MyRepository{}
}

func (r *MyRepository) GetHello() string {
	return "Hello World"
}
