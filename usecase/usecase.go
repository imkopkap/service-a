package usecase

import (
	r "gitlab.com/imkopkap/service-a/repository"
)

type Usecase interface {
	GetHello() string
}

type MyUsecase struct {
	repo r.Repository
}

func NewMyUsecase(repo r.Repository) Usecase {
	return &MyUsecase{
		repo: repo,
	}
}

func (u *MyUsecase) GetHello() string {
	return u.repo.GetHello()
}
